package ru.t1.skasabov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.enumerated.Role;

@NoArgsConstructor
public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull private static final String NAME = "logout";

    @NotNull private static final String DESCRIPTION = "Logout current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
