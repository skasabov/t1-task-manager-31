package ru.t1.skasabov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull private static final String NAME = "login";

    @NotNull private static final String DESCRIPTION = "User login.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
