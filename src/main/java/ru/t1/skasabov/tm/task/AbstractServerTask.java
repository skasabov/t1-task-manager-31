package ru.t1.skasabov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull protected Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}
