package ru.t1.skasabov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull protected Socket socket;

    public AbstractServerSocketTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server);
        this.socket = socket;
    }

}
