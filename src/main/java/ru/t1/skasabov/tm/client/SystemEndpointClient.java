package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.skasabov.tm.dto.request.ServerAboutRequest;
import ru.t1.skasabov.tm.dto.request.ServerVersionRequest;
import ru.t1.skasabov.tm.dto.response.ServerAboutResponse;
import ru.t1.skasabov.tm.dto.response.ServerVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println("[ABOUT]");
        System.out.println("name: " + serverAboutResponse.getEmail());
        System.out.println("email: " + serverAboutResponse.getName());
        System.out.println();

        @NotNull final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println("[VERSION]");
        System.out.println(serverVersionResponse.getVersion());

        client.disconnect();
    }

}
