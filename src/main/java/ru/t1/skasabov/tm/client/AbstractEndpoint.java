package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

@Setter
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull private String host = "localhost";

    @NotNull private Integer port = 6060;

    @NotNull private Socket socket;

    public AbstractEndpoint(@NotNull final String host, @NotNull final Integer port) {
        this.host = host;
        this.port = port;
    }

    @SneakyThrows
    @NotNull
    protected Object call(@NotNull final Object data) {
        getObjectOutputStream().writeObject(data);
        return getObjectInputStream().readObject();
    }

    @SneakyThrows
    @NotNull
    private ObjectOutputStream getObjectOutputStream() {
        return new ObjectOutputStream(getOutputStream());
    }

    @SneakyThrows
    @NotNull
    private ObjectInputStream getObjectInputStream() {
        return new ObjectInputStream(getInputStream());
    }

    @SneakyThrows
    @NotNull
    private OutputStream getOutputStream() {
        return socket.getOutputStream();
    }

    @SneakyThrows
    @NotNull
    private InputStream getInputStream() {
        return socket.getInputStream();
    }

    @SneakyThrows
    public void connect() {
        socket = new Socket(host, port);
    }

    @SneakyThrows
    public void disconnect() {
        socket.close();
    }

}
