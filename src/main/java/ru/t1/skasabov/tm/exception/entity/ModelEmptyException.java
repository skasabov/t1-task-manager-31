package ru.t1.skasabov.tm.exception.entity;

public final class ModelEmptyException extends AbstractEntityNotFoundException {

    public ModelEmptyException() {
        super("Error! Model is empty...");
    }

}
