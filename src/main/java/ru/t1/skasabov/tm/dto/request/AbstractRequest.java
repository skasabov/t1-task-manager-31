package ru.t1.skasabov.tm.dto.request;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public abstract class AbstractRequest implements Serializable {
}
