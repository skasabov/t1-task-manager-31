package ru.t1.skasabov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public final class ServerAboutResponse extends AbstractResponse {

    @NotNull private String email;

    @NotNull private String name;

}
